# ntp_client

Ansible role to configure a host to use time synchronization and set
timezone.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

* `ntp_client_using_timesyncd`

    Boolean that tells if systemd-timesyncd is used to handle time
    synchronization. This is the default, but in some setups time
    synchronization is setup in another way using e.g. chrony. This is
    the case with FreeIPA/RedHat IdM.

    If this value is false, the role will only ensure the time
    synchronization is enabled and set the correct timezone (using
    timedatectl).

    Default: `true`

* `ntp_servers`

    List of addresses to ntp servers to use for time synchronization.

    Default: The value of `network_ntp_servers` (or an empty list if
    that is not defined).

* `ntp_fallback_servers`

    List of fallback servers to use if the `ntp_servers` are not
    availble.

    Default: `[0,1,2,3].se.pool.ntp.org`

* `ntp_timezone`

    The timezone to use. Issue the command `timedatectl
    list-timezones` to get acceptable values.

    Default: `UTC`

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ntp_client
```

## License

BSD 2-clause
